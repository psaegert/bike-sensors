import sys
from util import *

DIR = './data/SensorLogger/'
FILENAME = 'data.csv'


if __name__ == "__main__":
    force = False
    if len(sys.argv) > 1:
        force = sys.argv[1] == "-f"
    print(f'Force Update: {force}')
    generate_datasets(DIR, FILENAME, force=force, verbose=1)