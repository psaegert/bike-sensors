import os
import numpy as np
import pandas as pd
from tqdm import tqdm
from time import sleep
from sklearn.preprocessing import StandardScaler
from scipy.signal import argrelextrema
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

bg = np.array(plt.get_cmap('viridis')(0)) / 2
bg[3] = 1



def load_sensors(path:str, interpolate:list=None, exclude:list=None, verbose:bool=True):
    if exclude == None: exclude = []
    if interpolate == None: interpolate = []

    if verbose: print('Reading Files...')
    sensors = {}
    for file in os.listdir(path):
        if not file.endswith('csv') or os.path.splitext(file)[0] in exclude: continue

        try:
            sensors[os.path.splitext(file)[0]] = pd.read_csv(os.path.join(path, file), index_col=False)
        except pd.errors.EmptyDataError:
            if os.path.splitext(file)[0] in interpolate:
                interpolate.remove(os.path.splitext(file)[0])
            print(f'Skipping {os.path.join(path, file)}: File is empty')

    if verbose: print('Clipping Start Time...')
    for k, r in sensors.items():
        sensors[k].drop(np.where(r['seconds_elapsed'] < 0)[0], inplace=True)
        sensors[k].reset_index(drop=True, inplace=True)

    if len(interpolate) > 0:
        total_length = max([len(v) for v in sensors.values()])
        if verbose: print('Interpolating...')
        sleep(.3)
        for k in interpolate:
            sensors[k] = interpolate_dataframe(sensors[k], fine_time_series=sensors['Accelerometer']['time'], total_length=total_length, keep_sparse_time=True)
    
    if verbose: print('Converting Time...')
    for k in sensors.keys():
        sensors[k]['Time'] = pd.to_datetime(sensors[k]['time'])
        sensors[k].drop(columns=['time'], inplace=True)
        sensors[k].columns = [f'{k}_{c}' for c in sensors[k]]
    
    return sensors


def interpolate_dataframe(sparse_dataframe, fine_time_series, total_length, keep_sparse_time=False, verbose=False):
    # create empty series
    sparse_columns = [c for c in sparse_dataframe.columns if (keep_sparse_time or c != 'time')]
    new_series_list = [pd.Series(np.full(total_length, np.nan), name=c) for c in sparse_columns]

    # match sparse time to closest fine time
    fine_index = 0
    sparse_tqdm = tqdm(sparse_dataframe['time'], desc='Timestep') if verbose else sparse_dataframe['time']
    if verbose: sleep(0.3)
    for i, sparse_time in enumerate(sparse_tqdm):
        new_closest_time_delta = np.abs(fine_time_series[fine_index] - sparse_time)
        last_closest_time_delta = new_closest_time_delta
        while new_closest_time_delta <= last_closest_time_delta:
            fine_index += 1
            last_closest_time_delta = new_closest_time_delta
            new_closest_time_delta = np.abs(fine_time_series[fine_index] - sparse_time)

        for j, c in enumerate(sparse_columns):
            if not keep_sparse_time and c == 'time': continue
            new_series_list[j].at[fine_index - 1] = sparse_dataframe[c][i]

    return pd.DataFrame({s.name: s.interpolate('linear') for s in new_series_list})


def combine_sensors(sensors: dict, SAMPLE_FREQ=200, clip=True, verbose=True):
    if clip:
        if verbose: print('Truncating...')
        last_useful_row = min([len(r) for r in sensors.values()])
        data = pd.concat(list(sensors.values()), axis=1)[:last_useful_row]
    else:
        data = pd.concat(list(sensors.values()), axis=1)

    if verbose: print('Averaging Time...')
    data['Time'] = data[[k for k in data.keys() if '_Time' in k]].mean(axis=1, numeric_only=False)
    data = data.drop(columns=[k for k in data.keys() if '_Time' in k]).dropna().reset_index(drop=True)

    if verbose: print('Averaging Seconds Elapsed...')
    data['Seconds_Elapsed'] = data[[k for k in data.keys() if '_seconds_elapsed' in k]].mean(axis=1, numeric_only=False)
    data = data.drop(columns=[k for k in data.keys() if '_seconds_elapsed' in k]).dropna().reset_index(drop=True)

    if verbose:
        print('Adding Derivatives...')
        sleep(0.3)
    data = add_pedal_seconds(data, verbose=verbose)
    data = add_pedal_force(data, verbose=verbose)

    if clip:
        data = clip_data(data, SAMPLE_FREQ=SAMPLE_FREQ)
        
    data['Seconds_Elapsed'] = data['Seconds_Elapsed'] - data['Seconds_Elapsed'][0]

    return data


def clip_data(data, SAMPLE_FREQ=200, smooth_window_size=None):
    if smooth_window_size is None:
        smooth_window_size = SAMPLE_FREQ*10
    smoothed_speed = smooth(data['Location_speed'], smooth_window_size)
    start_index = np.min(np.where(np.logical_and(smoothed_speed > 2, data['Location_horizontalAccuracy'][smooth_window_size // 2-1:-(smooth_window_size-smooth_window_size//2)] < 10)))
    start_index = start_index - smooth_window_size//2 - SAMPLE_FREQ

    end_index = np.max(np.where(smoothed_speed > 2))
    end_index = end_index + smooth_window_size // 2 - SAMPLE_FREQ * 7

    return data[start_index:end_index].reset_index(drop=True)


def add_pedal_seconds(data, SAMPLE_FREQ=200, smooth_window_size=None, verbose=False, peak_trigger=True):
    if smooth_window_size is None:
        smooth_window_size=SAMPLE_FREQ // 3
    gyro_data = StandardScaler().fit_transform(np.array(data['Gyroscope_z']).reshape(-1, 1)).reshape(-1)

    transfromed_gyro = (gyro_data + 1)**2
    transformed_smoothed_gyro = smooth(transfromed_gyro, smooth_window_size)
    threshold = np.mean(transformed_smoothed_gyro) + np.std(transformed_smoothed_gyro) / 3

    if peak_trigger:
        trigger_smooth = argrelextrema(transformed_smoothed_gyro, np.greater)[0]
        trigger = trigger_smooth[transformed_smoothed_gyro[trigger_smooth] > threshold]
    else:
        trigger = np.where(np.diff((transformed_smoothed_gyro > threshold).astype(int)) > 0)[0]

    timedeltas = [(data['Time'][t1] - data['Time'][t0]).total_seconds() for t0, t1 in zip(trigger[:-1], trigger[1:])]

    data['pedal_seconds'] = interpolate_dataframe(
        pd.DataFrame({'time': data['Time'][trigger + smooth_window_size // 2][1:], 'pedal_seconds': timedeltas}).reset_index(drop=True),
        data['Time'],
        len(data['Time']),
        False,
        verbose
    )

    return data


def add_pedal_force(data, SAMPLE_FREQ=200, smooth_window_size=None, verbose=False):
    if smooth_window_size is None:
        smooth_window_size = SAMPLE_FREQ // 3
    gyro_data = StandardScaler().fit_transform(np.array(data['Gyroscope_z']).reshape(-1, 1)).reshape(-1)
    transfromed_gyro = (gyro_data + 1)**2
    transformed_smoothed_gyro = smooth(transfromed_gyro, smooth_window_size)

    threshold = np.mean(transformed_smoothed_gyro) + np.std(transformed_smoothed_gyro) / 3
    trigger_smooth = argrelextrema(transformed_smoothed_gyro, np.greater)[0]
    trigger = trigger_smooth[transformed_smoothed_gyro[trigger_smooth] > threshold]

    data['pedal_force'] = interpolate_dataframe(
        pd.DataFrame({'time': data['Time'][trigger + smooth_window_size // 2], 'pedal_force': transformed_smoothed_gyro[trigger]}).reset_index(drop=True),
        data['Time'],
        len(data['Time']),
        False,
        verbose
    )

    return data


def load_all_data(path, filename, verbose=True):
    dataframe_list = []
    subfolder_tqdm = tqdm(os.listdir(path), desc='Loading') if verbose else os.listdir(path)
    if verbose: sleep(0.3)
    for subfolder in subfolder_tqdm:
        if not os.path.isfile(os.path.join(path, subfolder, filename)):
            if verbose: print(f'Generating "{os.path.join(path, subfolder, filename)}"...')
            data = combine_sensors(load_sensors(os.path.join(path, subfolder), interpolate=['Location'], exclude=[os.path.splitext(filename)[0], 'Metadata', 'AccelerometerUncalibrated', 'GyroscopeUncalibrated'], verbose=False), verbose=False)
            data.to_csv(os.path.join(path, subfolder, filename), index=False)
        dataframe_list.append(pd.read_csv(os.path.join(path, subfolder, filename), parse_dates=['Time'], index_col=False))
    
    for i, dataframe in enumerate(dataframe_list):
        dataframe['Recording'] = np.full(len(dataframe), i)

    return pd.concat(dataframe_list, ignore_index=True)


def generate_datasets(path, filename='data.csv', subfolder_list=None, verbose=1, force=False):
    if subfolder_list is None:
        subfolder_tqdm = tqdm(os.listdir(path), desc='Generating') if verbose > 0 else os.listdir(path)
    else:
        subfolder_tqdm = tqdm(subfolder_list, desc='Generating') if verbose > 0 else subfolder_list
    if verbose > 0: sleep(0.3)
    for subfolder in subfolder_tqdm:
        if not force and os.path.exists(os.path.join(path, subfolder, filename)):
            continue
        data = combine_sensors(load_sensors(os.path.join(path, subfolder), interpolate=['Location'], exclude=[os.path.splitext(filename)[0], 'Metadata', 'AccelerometerUncalibrated', 'GyroscopeUncalibrated'], verbose=(verbose == 2)), verbose=(verbose == 2))
        data.to_csv(os.path.join(path, subfolder, filename), index=False)


def smooth(array, smooth_window_size):
    return np.mean(np.lib.stride_tricks.sliding_window_view(array, window_shape=smooth_window_size), axis=1)


def sliding_std(array, smooth_window_size):
    return np.std(np.lib.stride_tricks.sliding_window_view(array, window_shape=smooth_window_size), axis=1)

def derive_gear_switches(data, sensitivity=0.0003):
    return np.where(np.abs(np.diff(np.diff(data['pedal_seconds']))) > sensitivity)[0]

def tolerant_mean_std(values):
    npvalues = np.ma.empty((len(values), np.max([len(v) for v in values])))
    npvalues.mask = True
    for i, v in enumerate(values):
        npvalues[i, :len(v)] = v

    return npvalues.mean(axis=0), npvalues.std(axis=0)

def hist2d(x, y, res=512, xlabel=None, ylabel=None, bins=None, lognorm=False, ax=None, crosshair=True, padding=0, weights=None):
    if type(res) == int:
        res = (res, res)
    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=(10, 10))
    ax.set_facecolor(bg)
    mask = np.logical_and(np.logical_not(np.isnan(x)), np.logical_not(np.isnan(y)))
    xspan = max(x[mask]) - min(x[mask])
    yspan = max(y[mask]) - min(y[mask])
    if bins is None: bins = [(min(x[mask]) - padding * xspan, max(x[mask]) + padding * xspan), (min(y[mask]) - padding * yspan, max(y[mask]) + padding * yspan)]
    ax.hist2d(
        x[mask],
        y[mask],
        bins=(np.linspace(*bins[0], res[0]), np.linspace(*bins[1], res[1])),
        norm=LogNorm() if lognorm else None,
        weights=weights[mask] if weights is not None else None
    )
    if crosshair:
        ax.axhline(0, color='white', alpha=0.05, linewidth=1)
        ax.axvline(0, color='white', alpha=0.05, linewidth=1)
    ax.set_xlabel(x.name if xlabel is None else xlabel); ax.set_ylabel(y.name if ylabel is None else ylabel)